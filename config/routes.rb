Rails.application.routes.draw do
  resources(:microposts, :users)

  root to: redirect('users')
end
